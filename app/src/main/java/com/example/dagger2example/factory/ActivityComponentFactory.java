package com.example.dagger2example.factory;

import com.example.dagger2example.MainActivity;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {WheelsModule.class, PetrolEngineModule.class})
public interface ActivityComponentFactory {
    Car getCar();
    void inject(MainActivity mainActivity);
    @Subcomponent.Factory
    interface Factory {
        ActivityComponentFactory create(@BindsInstance @Named("horse power") int horsePower,
                                        @BindsInstance @Named("engine capacity") int engineCapacity);
    }
}
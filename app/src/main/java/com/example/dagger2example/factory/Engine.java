package com.example.dagger2example.factory;


public interface Engine {

    void start();
}
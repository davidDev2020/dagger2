package com.example.dagger2example.factory;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = DriverModule.class)
public interface AppComponentFactory {
    ActivityComponentFactory.Factory getActivityComponentFactory();
    @Component.Factory
    interface Factory {
        AppComponentFactory create(DriverModule driverModule);
    }
}
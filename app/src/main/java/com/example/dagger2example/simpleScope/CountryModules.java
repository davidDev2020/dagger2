package com.example.dagger2example.simpleScope;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CountryModules {

    @Provides
//    @SimpleScope
    Country provideCountry(City city){
        return new Country(city);
    }
}

package com.example.dagger2example.simpleScope;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;
import javax.inject.Singleton;

@SimpleScope
public class Country {
    City city;
    @Inject
    public Country(City city) {
        this.city = city;

    }

    public void printName(){
        Log.d(Constant.TAG,"\n \n country: "+this+"\n city is:"+city);
    }
}

package com.example.dagger2example.simpleScope;

import javax.inject.Singleton;

import dagger.Component;

@Component
@SimpleScope
public interface CountryComponent {

    Country getCountry();
    People getPeople();
}

package com.example.dagger2example.simpleScope;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class People {

    @Inject
    public People() {
    }

    public void printName(){
        Log.d(Constant.TAG,"\n \n people: "+this );
    }
}

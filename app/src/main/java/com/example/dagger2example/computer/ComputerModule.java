package com.example.dagger2example.computer;

import com.example.dagger2example.computer.Computer;

import dagger.Module;
import dagger.Provides;

@Module
public class ComputerModule {

    private String name;

    public ComputerModule(String name) {
        this.name = name;
    }




    @Provides
    Computer provideComputer(){
        return new Computer(name);
    }
}

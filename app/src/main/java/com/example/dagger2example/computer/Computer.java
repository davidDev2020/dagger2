package com.example.dagger2example.computer;


import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Computer {

    private String name;


    public Computer(String name) {
        this.name = name;
    }

    public void writeYourName(){
        Log.d(Constant.TAG, "writeYourName: "+name);
    }
}

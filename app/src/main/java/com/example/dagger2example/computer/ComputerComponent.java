package com.example.dagger2example.computer;

import dagger.Component;
@Component(modules = ComputerModule.class)
public interface ComputerComponent {

    Computer getComputer();

}




package com.example.dagger2example.mobile;

import dagger.Component;

@Component(modules = SamsungModule.class)
public interface SmartPhoneComponent {


    SmartPhone getSmartPhone();

}

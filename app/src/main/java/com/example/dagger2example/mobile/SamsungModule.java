package com.example.dagger2example.mobile;

import com.example.dagger2example.mobile.Samsung;
import com.example.dagger2example.mobile.SmartPhone;

import dagger.Module;
import dagger.Provides;

@Module
public class SamsungModule {

    String message;

    public SamsungModule(String message) {
        this.message = message;
    }

    @Provides
    String provideMessage(){
        return message;
    }

    @Provides
    SmartPhone provideSmartPhone(Samsung samsung){
        return samsung;
    }
}

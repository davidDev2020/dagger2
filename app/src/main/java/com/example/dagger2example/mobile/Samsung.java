package com.example.dagger2example.mobile;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Samsung  implements SmartPhone{

    String message;

    @Inject
    public Samsung(String message) {
        this.message = message;
    }

    @Override
    public void sendMessage() {
        Log.d(Constant.TAG, "sendMessage: message from samsung:"+message);
    }
}

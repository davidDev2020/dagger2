package com.example.dagger2example.fruitSubComponentBuilder;

import javax.inject.Inject;
import javax.inject.Named;

public class TeamsB {
    String name;
    @Inject
    public TeamsB(@Named("TEAM_B")String name) {
        this.name = name;
    }
}

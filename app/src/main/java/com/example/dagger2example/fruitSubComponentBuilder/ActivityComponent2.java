package com.example.dagger2example.fruitSubComponentBuilder;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Subcomponent;

@ActivityAnnotation
@Subcomponent(modules = { RefreeModule.class})
public interface ActivityComponent2 {

    FootballMatch2 getFootballMatch();

    @Subcomponent.Builder
    interface Builder{
        @BindsInstance
        Builder setTeamA_Name(@Named("TEAM_A") String name);
        @BindsInstance
        Builder setTeamB_Name(@Named("TEAM_B") String name);

//        Builder appComponent(AppComponent2 appComponent); we delete this because it needs for component dependencies

        ActivityComponent2 build();
    }
}

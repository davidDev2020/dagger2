package com.example.dagger2example.fruitSubComponentBuilder;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = StadiumModule2.class)
public interface AppComponent2 {

    /////Stadium2 getStadium(); we dose'nt need it any more because
    // subComponent access to all object of parent components

    ActivityComponent2.Builder getActivityComponentBuilder();
}

package com.example.dagger2example.fruitSubComponentBuilder;

import javax.inject.Inject;
import javax.inject.Named;

public class TeamsA {
    String name;
    @Inject
    public TeamsA(@Named("TEAM_A")String name) {
        this.name = name;
    }
}

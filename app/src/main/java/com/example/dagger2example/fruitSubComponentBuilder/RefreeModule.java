package com.example.dagger2example.fruitSubComponentBuilder;

import dagger.Module;
import dagger.Provides;

@Module
public  class RefreeModule {

    @Provides
    Referee2 bindReferee(){
     return new Referee2();
    }
}

package com.example.dagger2example.AndroidInjection;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import javax.inject.Inject;


public class ViewModelProviderFactory extends ViewModelProvider.NewInstanceFactory {

    private final Repository  dataManager;


    @Inject
    public ViewModelProviderFactory(Repository dataManager) {
        this.dataManager = dataManager;

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DetailViewModel.class)) {
            //noinspection unchecked
            return (T) new DetailViewModel(dataManager);
        }
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
package com.example.dagger2example.AndroidInjection;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {


    @ContributesAndroidInjector
    public abstract DetailActivity getDetailActivity();


}

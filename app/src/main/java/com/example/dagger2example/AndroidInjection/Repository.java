package com.example.dagger2example.AndroidInjection;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Repository {

    @Inject
    public Repository() {
    }

    String getName(){
        return "my name is David younesi and it is getted from Api service";
    }
}

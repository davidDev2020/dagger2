package com.example.dagger2example.AndroidInjection;



import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.ViewModelProviders;

import com.example.dagger2example.Constant;
import com.example.dagger2example.R;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class DetailActivity extends DaggerAppCompatActivity {

    @Inject
    String text;
    @Inject
    ViewModelProviderFactory factory;

    DetailViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_injection);

        viewModel = ViewModelProviders.of(this, factory).get(DetailViewModel.class);

        Log.d(Constant.TAG, "the text message: "+text);
        Log.d(Constant.TAG, "text from view model: "+viewModel.getName());


    }
}

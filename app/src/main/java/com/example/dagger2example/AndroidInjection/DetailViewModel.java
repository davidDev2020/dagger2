package com.example.dagger2example.AndroidInjection;

import androidx.lifecycle.ViewModel;

public class DetailViewModel extends ViewModel {

    private Repository repository;
    public DetailViewModel(Repository repository) {
        this.repository = repository;
    }

    String getName(){
        return repository.getName();
    }
}

package com.example.dagger2example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.example.dagger2example.AndroidInjection.DetailActivity;
import com.example.dagger2example.appComponent.DaggerPerActivityComponentFootballMatch;
import com.example.dagger2example.appComponent.FootballMatch;
import com.example.dagger2example.appComponent.PerActivityComponentFootballMatch;
import com.example.dagger2example.car.Car;
import com.example.dagger2example.car.CarComponent;
import com.example.dagger2example.car.DaggerCarComponent;
import com.example.dagger2example.computer.Computer;
import com.example.dagger2example.computer.ComputerComponent;
import com.example.dagger2example.computer.ComputerModule;
import com.example.dagger2example.computer.DaggerComputerComponent;
import com.example.dagger2example.factory.ActivityComponentFactory;
import com.example.dagger2example.fruit.DaggerFruitComponent;
import com.example.dagger2example.fruit.Fruit;
import com.example.dagger2example.fruit.FruitComponent;
import com.example.dagger2example.fruitSubComponentBuilder.ActivityComponent2;
import com.example.dagger2example.mobile.DaggerSmartPhoneComponent;
import com.example.dagger2example.mobile.SamsungModule;
import com.example.dagger2example.mobile.SmartPhone;
import com.example.dagger2example.mobile.SmartPhoneComponent;
import com.example.dagger2example.simpleScope.Country;
import com.example.dagger2example.simpleScope.CountryComponent;
import com.example.dagger2example.simpleScope.DaggerCountryComponent;
import com.example.dagger2example.subComponent.ActivityComponent;
import com.example.dagger2example.subComponent.DaggerAppComponent;
import com.example.dagger2example.subComponent.TeamsModule;
import com.example.dagger2example.test.DaggerTestComponent;
import com.example.dagger2example.test.TestComponent;
import com.example.dagger2example.user.DaggerUserComponent;
import com.example.dagger2example.user.User;
import com.example.dagger2example.user.UserComponent;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    Car car;
    @Inject
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        injectScope();
//        subComponent();
//        subComponentBuilder();
//        subComponentFactory();
//        testQualifires();

    }

    public void buttonOne(View view) {
//        runtimeInjection();
//        constructorInjection();
//        runtimeInjectionWithParameter();
//        injectWithBuilderInterface();
//        singletonExample();
//      injectScope();
//        simpleScope();
//        subComponent();
//        subComponent();
        testQualifires();
  }


    void constructorInjection(){

        CarComponent carComponent = DaggerCarComponent.create();
        car = carComponent.getCar();
        car.drive();
    }

    void singletonExample(){

        CarComponent carComponent1 = DaggerCarComponent.create();
        Log.d(Constant.TAG, "First Component First Car: ");
        Car car1 = carComponent1.getCar();
        car1.drive();
        Log.d(Constant.TAG, "First Component Second  Car: ");
        Car car2 = carComponent1.getCar();
        car2.drive();


        CarComponent carComponent2 = DaggerCarComponent.create();
        Log.d(Constant.TAG, "Second Component: First car ");
        Car car3 = carComponent2.getCar();
        car3.drive();

        Log.d(Constant.TAG, "Second Component: Second car ");
        Car car4 = carComponent2.getCar();
        car4.drive();


    }

    void fieldInjection(){
        UserComponent component = DaggerUserComponent.create();
        component.injectMyUser(this);
        user.whatsyourname();

    }

//    void moduleInjection(){
//        HouseComponent component = DaggerHouseComponent.create();
//        House house = component.getHouse();
//        house.buildTheHouse();
//    }

    void runtimeInjection(){
        ComputerComponent component = DaggerComputerComponent.builder()
                .computerModule(new ComputerModule("ASUS"))
                .build();
        Computer computer = component.getComputer();
        computer.writeYourName();
    }


    void runtimeInjectionWithParameter(){

        SmartPhoneComponent component = DaggerSmartPhoneComponent.builder()
                .samsungModule(new SamsungModule("hello from davood"))
                .build();
        SmartPhone smartPhone = component.getSmartPhone();
        smartPhone.sendMessage();

    }


    void injectWithBuilderInterface(){

        FruitComponent component = DaggerFruitComponent.builder()
                .fruitname("Apple")
                .fruitColor("green")
                .build();
        Fruit apple = component.getFruit();
        Fruit apple2 = component.getFruit();

        Log.d(Constant.TAG, "is apple1 same as apple2 : " + apple.equals(apple2));


        apple.printName();
    }

    void injectScope(){
        // inject with preactivity and application and scope
        Log.d(Constant.TAG,"\n \n *********** injectScope *********************");
        PerActivityComponentFootballMatch component = DaggerPerActivityComponentFootballMatch.builder()
                .appComponentFootballMatch(G.getInstance().getAppComponent()).
                build();
       FootballMatch footballMatch =  component.getFootballMatch();
       FootballMatch footballMatch2 =  component.getFootballMatch();
       footballMatch.printNames();
       footballMatch2.printNames();



    }

    void simpleScope(){

        Log.d(Constant.TAG,"\n \n *********** Simple Scope *********************");
        CountryComponent component = DaggerCountryComponent.create();

        component.getCountry().printName();
       Country country =  component.getCountry();
       country.printName();
        component =null;
        country.printName();

        Log.d(Constant.TAG,"\n \n componnent2");
        CountryComponent component2 = DaggerCountryComponent.create();
        component2.getCountry().printName();

        Log.d(Constant.TAG,"\n \n********** People**********");
        component2.getPeople().printName();
        component2.getPeople().printName();


    }

    void subComponent(){

        ActivityComponent component =  G.getInstance().getAppComponent_subComponent()
                .getActivityComponent(new TeamsModule("team A name","team B name"));
        component.getFootballMatch().printNames();
        component.getFootballMatch().printNames();

    }

    void subComponentBuilder(){

        ActivityComponent2 component = G.getInstance().getAppComponent2()
                .getActivityComponentBuilder()
                .setTeamA_Name("team a name")
                .setTeamB_Name("team b name")
                .build();
        component.getFootballMatch().printNames();
        component.getFootballMatch().printNames();

    }

    void subComponentFactory(){
        ActivityComponentFactory component = G.getInstance().getAppComponentFactory()
                .getActivityComponentFactory().create(150, 1400);

      com.example.dagger2example.factory.Car  car1=  component.getCar();
      com.example.dagger2example.factory.Car  car2=  component.getCar();

        car1.drive();
        car2.drive();
    }

    void testQualifires(){

        TestComponent component = DaggerTestComponent.create();
        Log.d(Constant.TAG, "test: "+component.getUser().toString());

    }


    public void ActivityInject(View view) {

        startActivity(new Intent(this, DetailActivity.class));

    }
}

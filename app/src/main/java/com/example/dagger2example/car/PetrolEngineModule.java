package com.example.dagger2example.car;

import com.example.dagger2example.car.Engine;
import com.example.dagger2example.car.PetrolEngine;

import dagger.Module;
import dagger.Provides;

@Module
public class PetrolEngineModule {


    @Provides
    Engine provideEngine(PetrolEngine petrolEngine){
        return  petrolEngine;
    }
}

package com.example.dagger2example.car;

import com.example.dagger2example.car.DieselEngine;
import com.example.dagger2example.car.Engine;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class DieselEngineModule {



    @Binds
    abstract Engine bindEngine(DieselEngine dieselEngine);

}

package com.example.dagger2example.car;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Car {

    private static final String TAG = "CarTAG";

    private Wheels wheels;
    private Engine engine;
    private Driver driver;

    @Inject Color color;


    @Inject
    public Car(Wheels wheels, Engine engine,Driver driver) {
        Log.d(Constant.TAG, "Car constructor");
        this.wheels = wheels;
        this.engine = engine;
        this.driver =driver;
    }


    public void drive(){
        engine.start();
        Log.d(Constant.TAG, "driving ...");
        Log.d(Constant.TAG, "engine is "+engine);
        Log.d(Constant.TAG, "driver is "+driver);



    }

    @Inject
    public void setListener(Remote remote){
        remote.enableRemote(this);
    }

}

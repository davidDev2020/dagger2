package com.example.dagger2example.car;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Remote {

    @Inject
    public Remote() {
    }

    public void enableRemote(Car car){
        Log.d(Constant.TAG, "enableRemote: ");
    }
}

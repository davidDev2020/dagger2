package com.example.dagger2example.car;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Color {


    @Inject
    public Color() {
        Log.d(Constant.TAG, "Color: color is created");
    }
}

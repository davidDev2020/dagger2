package com.example.dagger2example.car;



import javax.inject.Singleton;

import dagger.Component;
@Singleton
//@Component(modules = PetrolEngineModule.class)
@Component(modules = DieselEngineModule.class)
public interface CarComponent {

    Car getCar();

}

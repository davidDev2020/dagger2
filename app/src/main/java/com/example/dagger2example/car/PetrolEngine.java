package com.example.dagger2example.car;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class PetrolEngine implements Engine {

    @Inject
    public PetrolEngine() {
    }

    @Override
    public void start() {
        Log.d(Constant.TAG, "PetrolEngine started: ");
    }
}

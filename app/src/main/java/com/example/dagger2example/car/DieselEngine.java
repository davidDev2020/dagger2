package com.example.dagger2example.car;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DieselEngine implements Engine {

    @Inject
    public DieselEngine() {
    }

    @Override
    public void start() {
        Log.d(Constant.TAG, "DieselEngine started: ");
    }
}

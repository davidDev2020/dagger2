package com.example.dagger2example.fruit;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;
import javax.inject.Named;

public class Apple implements Fruit {

    private  String name;
    private String color;

    @Inject
    public Apple(@Named("NAME") String name,@Named("COLOR") String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public void printName() {

        Log.d(Constant.TAG, "printName: \n fruit nameis :"+ name +"\nfruit color is :"+color);
    }
}

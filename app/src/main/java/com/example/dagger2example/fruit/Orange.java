package com.example.dagger2example.fruit;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class Orange implements Fruit {

    private  String name;

    @Inject
    public Orange(String name) {
        this.name = name;
    }

    @Override
    public void printName() {

        Log.d(Constant.TAG, "printName: fruit name:"+ name);
    }
}

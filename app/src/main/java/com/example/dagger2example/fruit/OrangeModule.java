package com.example.dagger2example.fruit;


import dagger.Binds;
import dagger.Module;

@Module
public abstract class OrangeModule {



    @Binds
    abstract Fruit  provideFruit(Orange apple);
}

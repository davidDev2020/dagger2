package com.example.dagger2example.fruit;


import com.example.dagger2example.fruit.Apple;
import com.example.dagger2example.fruit.Fruit;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class AppleModule {



    @Binds
    abstract Fruit  provideFruit(Apple apple);
}

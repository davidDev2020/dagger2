package com.example.dagger2example.fruit;

import javax.inject.Named;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = AppleModule.class)
public interface FruitComponent {

    Fruit getFruit();


    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder fruitname(@Named("NAME") String name);
        @BindsInstance
        Builder fruitColor(@Named("COLOR") String color);

        FruitComponent build();
    }
}

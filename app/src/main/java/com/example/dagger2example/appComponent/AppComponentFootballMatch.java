package com.example.dagger2example.appComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppComponentFootballMatchModule.class)
public interface AppComponentFootballMatch {

    Stadium getStadium();
}

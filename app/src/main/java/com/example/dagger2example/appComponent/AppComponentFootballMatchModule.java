package com.example.dagger2example.appComponent;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppComponentFootballMatchModule {

    @Singleton
    @Provides
    Stadium provideStatuim(){
        return new Stadium();
    }
}

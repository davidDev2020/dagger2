package com.example.dagger2example.appComponent;

import dagger.Component;

@Component(dependencies = AppComponentFootballMatch.class)
@PerActivityAnnotation
public interface PerActivityComponentFootballMatch {

    FootballMatch getFootballMatch();
}

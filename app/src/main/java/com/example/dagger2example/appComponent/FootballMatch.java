package com.example.dagger2example.appComponent;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

@PerActivityAnnotation
public class FootballMatch {

    private MedicalTeam medicalTeam;
    private Referee referee;
    private Stadium stadium;
    private Teams teams;

    @Inject
    public FootballMatch(MedicalTeam medicalTeam, Referee referee, Stadium stadium, Teams teams) {
        this.medicalTeam = medicalTeam;
        this.referee = referee;
        this.stadium = stadium;
        this.teams = teams;
    }

   public void printNames(){
        Log.d(Constant.TAG, "printNames: \n" +
                "teams : "+teams+"\n"+
                "referee : " +referee+"\n"  +
                "stadium : "+stadium+"\n"+
                "medicalTeam :"+medicalTeam);
    }
}

package com.example.dagger2example;

import android.app.Activity;
import android.app.Application;

import com.example.dagger2example.appComponent.AppComponentFootballMatch;
import com.example.dagger2example.appComponent.DaggerAppComponentFootballMatch;
import com.example.dagger2example.factory.AppComponentFactory;
import com.example.dagger2example.factory.DaggerAppComponentFactory;
import com.example.dagger2example.factory.DriverModule;
import com.example.dagger2example.fruitSubComponentBuilder.AppComponent2;
import com.example.dagger2example.fruitSubComponentBuilder.DaggerAppComponent2;
import com.example.dagger2example.subComponent.AppComponent;
import com.example.dagger2example.subComponent.DaggerAppComponent;


import dagger.android.AndroidInjector;

import dagger.android.support.DaggerApplication;


public class G extends DaggerApplication {

    private static G instance;
    private AppComponentFootballMatch appComponent;
    private AppComponent appComponent_subComponent;
    private AppComponent2 appComponent2;
    private AppComponentFactory appComponentFactory;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = DaggerAppComponentFootballMatch.create();
        appComponent_subComponent = DaggerAppComponent.create();
        appComponent2 = DaggerAppComponent2.create();
        appComponentFactory = DaggerAppComponentFactory.factory().create(new DriverModule("Hans"));
    }
    public static G getInstance() {
        return instance;
    }

    public AppComponentFootballMatch getAppComponent() {
        return appComponent;
    }

    public AppComponent getAppComponent_subComponent() {
        return appComponent_subComponent;
    }


    public AppComponent2 getAppComponent2() {
        return appComponent2;
    }

    public AppComponentFactory getAppComponentFactory() {
        return appComponentFactory;
    }


    //for a
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return com.example.dagger2example.AndroidInjection.DaggerAppComponent
                .builder().application(this).build();
    }
}//END CLASS

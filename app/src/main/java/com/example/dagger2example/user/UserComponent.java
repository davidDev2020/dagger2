package com.example.dagger2example.user;


import com.example.dagger2example.MainActivity;



import dagger.Component;

@Component
public interface UserComponent {
    void injectMyUser(MainActivity activity);
}

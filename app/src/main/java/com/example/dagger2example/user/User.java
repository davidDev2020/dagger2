package com.example.dagger2example.user;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

public class User {

    private Address address;

    @Inject
    public User(Address address) {
        this.address = address;
    }

    public void whatsyourname(){
        Log.d(Constant.TAG, "whatsyourname: ");
    }


}

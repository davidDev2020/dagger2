package com.example.dagger2example.test;

import dagger.Component;

@Component(modules = UserModules.class)
public interface TestComponent {
    User getUser();
}

package com.example.dagger2example.test;

import dagger.Module;
import dagger.Provides;
@Module
public class UserModules {

    @Provides
    @UserFirstName
    String getFirsName(){
        return "david";
    }


    @Provides
    @UserLastName
    String getLastName(){
        return "younesi";
    }
}

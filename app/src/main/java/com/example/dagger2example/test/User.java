package com.example.dagger2example.test;

import javax.inject.Inject;

public class User {
    String firstName;
    String lastName;

    @Inject
    public User(@UserFirstName String firstName, @UserLastName String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}

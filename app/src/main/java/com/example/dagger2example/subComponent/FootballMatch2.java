package com.example.dagger2example.subComponent;

import android.util.Log;

import com.example.dagger2example.Constant;

import javax.inject.Inject;

@ActivityAnnotation
public class FootballMatch2 {
    private MedicalTeam2 medicalTeam2;
    private Referee2 referee;
    private Stadium2 stadium2;
    private TeamsA teamsA;
    private TeamsB teamsB;
    @Inject
    public FootballMatch2(MedicalTeam2 medicalTeam2, Referee2 referee,
                          Stadium2 stadium2, TeamsA teamsA,TeamsB teamsB
    ) {
        this.medicalTeam2 = medicalTeam2;
        this.referee = referee;
        this.stadium2 = stadium2;
        this.teamsA = teamsA;
        this.teamsB = teamsB;
    }
   public void printNames(){
        Log.d(Constant.TAG, "_______________________\n " +
                "printNames: \n" +
                "teamsA : "+ teamsA.name +"  _  "+ teamsA +"\n"+
                "teamsB : "+ teamsB.name +"  _  "+ teamsB +"\n"+
                "referee : " +referee+"\n"  +
                "stadium2 : "+ stadium2 +"\n"+
                "medicalTeam2 :"+ medicalTeam2+
                "\n _____________________");
    }
}

package com.example.dagger2example.subComponent;

import dagger.Subcomponent;

@ActivityAnnotation
@Subcomponent(modules = {TeamsModule.class,RefreeModule.class})
public interface ActivityComponent {

    FootballMatch2 getFootballMatch();

//    @Subcomponent.Builder
//    interface Builder{
//        @BindsInstance
//        Builder setTeamA_Name(@Named("TEAM_A") String name);
//        @BindsInstance
//        Builder setTeamB_Name(@Named("TEAM_B") String name);
//
//        ActivityComponent2 build();
//    }
}

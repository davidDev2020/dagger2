package com.example.dagger2example.subComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = StadiumModule2.class)
public interface AppComponent {

    /////Stadium2 getStadium(); we dose'nt need it any more because
    // subComponent access to all object of parent components

    ActivityComponent getActivityComponent(TeamsModule teamsModule);
}

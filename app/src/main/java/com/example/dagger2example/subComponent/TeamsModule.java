package com.example.dagger2example.subComponent;

import dagger.Module;
import dagger.Provides;

@Module
public class TeamsModule {

    String teamA_Name;
    String teamB_Name;

    public TeamsModule(String teamA_Name, String teamB_Name) {
        this.teamA_Name = teamA_Name;
        this.teamB_Name = teamB_Name;
    }


    @Provides
    TeamsA getTeamA(){
        return new TeamsA(teamA_Name);
    }

    @Provides
    TeamsB getTeamB(){
        return new TeamsB(teamB_Name);
    }

}

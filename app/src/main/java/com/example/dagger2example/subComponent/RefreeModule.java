package com.example.dagger2example.subComponent;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public  class RefreeModule {

    @Provides
     Referee2 bindReferee(){
     return new Referee2();
    }
}

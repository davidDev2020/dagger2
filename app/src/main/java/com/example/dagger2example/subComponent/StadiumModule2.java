package com.example.dagger2example.subComponent;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StadiumModule2 {

    @Singleton
    @Provides
    Stadium2 provideStatuim(){
        return new Stadium2();
    }
}

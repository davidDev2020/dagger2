package com.example.mylibrary;

import android.util.Log;



public class House {
    private Glass glass;
    private Door door;

    public static final String TAG = "ModuleInjectionTest";
    public House(Glass glass, Door door) {
        this.glass = glass;
        this.door = door;
    }

  public  void buildTheHouse(){
        Log.d(TAG, "buildTheHouse is started ");
    }
}
